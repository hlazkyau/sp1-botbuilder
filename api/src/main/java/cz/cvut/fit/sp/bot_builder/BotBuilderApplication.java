package cz.cvut.fit.sp.bot_builder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotBuilderApplication {

    public static void main(String[] args) {
        SpringApplication.run(BotBuilderApplication.class, args);
    }

}
